﻿namespace WindowsFormsApplication1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPatient = new System.Windows.Forms.Button();
            this.PatientSlidingScaleButton = new System.Windows.Forms.Button();
            this.BloodGlucoseResultButton = new System.Windows.Forms.Button();
            this.PatientDosageButton = new System.Windows.Forms.Button();
            this.DosageScheduleButton = new System.Windows.Forms.Button();
            this.PatientHistoryButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPatient
            // 
            this.btnPatient.Location = new System.Drawing.Point(60, 34);
            this.btnPatient.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPatient.Name = "btnPatient";
            this.btnPatient.Size = new System.Drawing.Size(187, 35);
            this.btnPatient.TabIndex = 1;
            this.btnPatient.Text = "Patient";
            this.btnPatient.UseVisualStyleBackColor = true;
            this.btnPatient.Click += new System.EventHandler(this.btnPatient_Click);
            // 
            // PatientSlidingScaleButton
            // 
            this.PatientSlidingScaleButton.Location = new System.Drawing.Point(303, 168);
            this.PatientSlidingScaleButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PatientSlidingScaleButton.Name = "PatientSlidingScaleButton";
            this.PatientSlidingScaleButton.Size = new System.Drawing.Size(187, 35);
            this.PatientSlidingScaleButton.TabIndex = 6;
            this.PatientSlidingScaleButton.Text = "Patient Sliding Scale";
            this.PatientSlidingScaleButton.UseVisualStyleBackColor = true;
            // 
            // BloodGlucoseResultButton
            // 
            this.BloodGlucoseResultButton.Location = new System.Drawing.Point(60, 168);
            this.BloodGlucoseResultButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BloodGlucoseResultButton.Name = "BloodGlucoseResultButton";
            this.BloodGlucoseResultButton.Size = new System.Drawing.Size(187, 35);
            this.BloodGlucoseResultButton.TabIndex = 5;
            this.BloodGlucoseResultButton.Text = "Blood Glucose Result";
            this.BloodGlucoseResultButton.UseVisualStyleBackColor = true;
            // 
            // PatientDosageButton
            // 
            this.PatientDosageButton.Location = new System.Drawing.Point(303, 103);
            this.PatientDosageButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PatientDosageButton.Name = "PatientDosageButton";
            this.PatientDosageButton.Size = new System.Drawing.Size(187, 35);
            this.PatientDosageButton.TabIndex = 4;
            this.PatientDosageButton.Text = "Patient Dosage";
            this.PatientDosageButton.UseVisualStyleBackColor = true;
            // 
            // DosageScheduleButton
            // 
            this.DosageScheduleButton.Location = new System.Drawing.Point(60, 101);
            this.DosageScheduleButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.DosageScheduleButton.Name = "DosageScheduleButton";
            this.DosageScheduleButton.Size = new System.Drawing.Size(187, 35);
            this.DosageScheduleButton.TabIndex = 3;
            this.DosageScheduleButton.Text = "Dosage Schedule";
            this.DosageScheduleButton.UseVisualStyleBackColor = true;
            // 
            // PatientHistoryButton
            // 
            this.PatientHistoryButton.Location = new System.Drawing.Point(303, 37);
            this.PatientHistoryButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PatientHistoryButton.Name = "PatientHistoryButton";
            this.PatientHistoryButton.Size = new System.Drawing.Size(187, 35);
            this.PatientHistoryButton.TabIndex = 2;
            this.PatientHistoryButton.Text = "Patient History";
            this.PatientHistoryButton.UseVisualStyleBackColor = true;
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(188, 235);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(187, 35);
            this.ExitButton.TabIndex = 7;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 305);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.PatientHistoryButton);
            this.Controls.Add(this.DosageScheduleButton);
            this.Controls.Add(this.PatientDosageButton);
            this.Controls.Add(this.BloodGlucoseResultButton);
            this.Controls.Add(this.PatientSlidingScaleButton);
            this.Controls.Add(this.btnPatient);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPatient;
        private System.Windows.Forms.Button PatientSlidingScaleButton;
        private System.Windows.Forms.Button BloodGlucoseResultButton;
        private System.Windows.Forms.Button PatientDosageButton;
        private System.Windows.Forms.Button DosageScheduleButton;
        private System.Windows.Forms.Button PatientHistoryButton;
        private System.Windows.Forms.Button ExitButton;
    }
}

