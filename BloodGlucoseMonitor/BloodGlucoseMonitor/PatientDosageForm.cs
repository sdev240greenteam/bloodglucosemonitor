﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BloodGlucoseMonitorWin
{
    public partial class PatientDosage : Form
    {
        public PatientDosage()
        {
            InitializeComponent();
        }

        private void GetDosageButton_Click(object sender, EventArgs e)
        {
            DosageAmountLabel.Text = 0.ToString();//MAKE THIS THE RESULT***CHANGE****
            GetDosage();
        }

        private void GetDosage()
        {

            int patientId;
            double glucoseLevel;

            //CHECK IF THE VALUES ENTERED ARE VALID
            if (Int32.TryParse(PatientIdTextBox.Text, out patientId))
            {
                if (double.TryParse(GlucoseLevelTextBox.Text, out glucoseLevel))
                {
                    //--------------------------------------------------------

                    //CREATES CLASS SO IT IS READY TO BE ENTERED INTO THE DATABASE
                    PatientDosage dosage = new PatientDosage
                    {
                        BaseGlucoseLevel = Double.Parse(GlucoseLevelLabel.Text),
                        PatientID = Int32.Parse(PatientIdTextBox.Text),
                        BeginDate = BeginDateCalender.SelectionRange.Start,
                        EndDate = EndDateCalender.SelectionRange.Start
                    };
                    //ALWAYS MAKE SURE IF ENTERED CORRECTLY, LABELS ARE BLACK
                    GlucoseLevelLabel.ForeColor = Color.Black;
                    PatientIdLabel.ForeColor = Color.Black;
                }
                //IF INVALID, MAKES THE COLORS OF LABELS RED TO INDICATE WRONG INPUT
                else
                {
                    GlucoseLevelLabel.ForeColor = Color.Red;
                }
            }
            else
            {
                PatientIdLabel.ForeColor = Color.Red;
            }
            //-------------------------------------------------

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            
        }

        private void NewButton_Click(object sender, EventArgs e)
        {
            //CLEARS VALUES TO ALLOW NEW ENTRY
            GlucoseLevelLabel.Text = "";
            PatientIdTextBox.Text = "";
        }
    }
}
