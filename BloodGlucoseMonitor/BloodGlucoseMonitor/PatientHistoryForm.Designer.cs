﻿namespace BloodGlucoseMonitorWin
{
    partial class PatientHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ResultLabel = new System.Windows.Forms.Label();
            this.OkButton = new System.Windows.Forms.Button();
            this.PatientIdTextBox = new System.Windows.Forms.TextBox();
            this.PatientIdLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PatientCalender = new System.Windows.Forms.MonthCalendar();
            this.ResultButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.DosageScheduleIdTextBox = new System.Windows.Forms.TextBox();
            this.NextButton = new System.Windows.Forms.Button();
            this.PreviousButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.NewButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResultLabel.Location = new System.Drawing.Point(12, 319);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(79, 25);
            this.ResultLabel.TabIndex = 3;
            this.ResultLabel.Text = "Result";
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(372, 351);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 8;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // PatientIdTextBox
            // 
            this.PatientIdTextBox.Location = new System.Drawing.Point(96, 14);
            this.PatientIdTextBox.Name = "PatientIdTextBox";
            this.PatientIdTextBox.Size = new System.Drawing.Size(75, 20);
            this.PatientIdTextBox.TabIndex = 0;
            // 
            // PatientIdLabel
            // 
            this.PatientIdLabel.AutoSize = true;
            this.PatientIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatientIdLabel.Location = new System.Drawing.Point(12, 14);
            this.PatientIdLabel.Name = "PatientIdLabel";
            this.PatientIdLabel.Size = new System.Drawing.Size(78, 18);
            this.PatientIdLabel.TabIndex = 9;
            this.PatientIdLabel.Text = "Patient Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Date";
            // 
            // PatientCalender
            // 
            this.PatientCalender.Location = new System.Drawing.Point(69, 119);
            this.PatientCalender.MaxSelectionCount = 1;
            this.PatientCalender.Name = "PatientCalender";
            this.PatientCalender.TabIndex = 11;
            // 
            // ResultButton
            // 
            this.ResultButton.Location = new System.Drawing.Point(15, 293);
            this.ResultButton.Name = "ResultButton";
            this.ResultButton.Size = new System.Drawing.Size(75, 23);
            this.ResultButton.TabIndex = 12;
            this.ResultButton.Text = "Get Result";
            this.ResultButton.UseVisualStyleBackColor = true;
            this.ResultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Dosage Schedule Id";
            // 
            // DosageScheduleIdTextBox
            // 
            this.DosageScheduleIdTextBox.Location = new System.Drawing.Point(142, 67);
            this.DosageScheduleIdTextBox.Name = "DosageScheduleIdTextBox";
            this.DosageScheduleIdTextBox.Size = new System.Drawing.Size(29, 20);
            this.DosageScheduleIdTextBox.TabIndex = 14;
            // 
            // NextButton
            // 
            this.NextButton.Location = new System.Drawing.Point(308, 229);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(75, 23);
            this.NextButton.TabIndex = 15;
            this.NextButton.Text = "Next";
            this.NextButton.UseVisualStyleBackColor = true;
            // 
            // PreviousButton
            // 
            this.PreviousButton.Location = new System.Drawing.Point(308, 258);
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(75, 23);
            this.PreviousButton.TabIndex = 16;
            this.PreviousButton.Text = "Previous";
            this.PreviousButton.UseVisualStyleBackColor = true;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(308, 200);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 17;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            // 
            // NewButton
            // 
            this.NewButton.Location = new System.Drawing.Point(308, 142);
            this.NewButton.Name = "NewButton";
            this.NewButton.Size = new System.Drawing.Size(75, 23);
            this.NewButton.TabIndex = 18;
            this.NewButton.Text = "New";
            this.NewButton.UseVisualStyleBackColor = true;
            this.NewButton.Click += new System.EventHandler(this.NewButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(308, 171);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 19;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // PatientHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 386);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.NewButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.PreviousButton);
            this.Controls.Add(this.NextButton);
            this.Controls.Add(this.DosageScheduleIdTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ResultButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PatientCalender);
            this.Controls.Add(this.PatientIdTextBox);
            this.Controls.Add(this.PatientIdLabel);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ResultLabel);
            this.Name = "PatientHistory";
            this.Text = "Patient History";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.TextBox PatientIdTextBox;
        private System.Windows.Forms.Label PatientIdLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MonthCalendar PatientCalender;
        private System.Windows.Forms.Button ResultButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox DosageScheduleIdTextBox;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button PreviousButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button NewButton;
        private System.Windows.Forms.Button SaveButton;
    }
}