﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodGlucoseMonitorWin
{
    partial class PatientDosage
    {
        public int PatientID { get; set; }
        public double BaseGlucoseLevel { get; set; }
        public double BaseDosageAmount { get; set; }
        public int SlidingScaleID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
