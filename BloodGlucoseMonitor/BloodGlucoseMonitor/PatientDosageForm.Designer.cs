﻿namespace BloodGlucoseMonitorWin
{
    partial class PatientDosage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PatientDosageTitleLabel = new System.Windows.Forms.Label();
            this.DosageTextLabel = new System.Windows.Forms.Label();
            this.DosageAmountLabel = new System.Windows.Forms.Label();
            this.BeginDateCalender = new System.Windows.Forms.MonthCalendar();
            this.BeginDateLabel = new System.Windows.Forms.Label();
            this.EndDateLabel = new System.Windows.Forms.Label();
            this.EndDateCalender = new System.Windows.Forms.MonthCalendar();
            this.PatientIdTextBox = new System.Windows.Forms.TextBox();
            this.GlucoseLevelTextBox = new System.Windows.Forms.TextBox();
            this.PatientIdLabel = new System.Windows.Forms.Label();
            this.GlucoseLevelLabel = new System.Windows.Forms.Label();
            this.GetDosageButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.PreviousButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.NewButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PatientDosageTitleLabel
            // 
            this.PatientDosageTitleLabel.AutoSize = true;
            this.PatientDosageTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PatientDosageTitleLabel.Location = new System.Drawing.Point(12, 9);
            this.PatientDosageTitleLabel.Name = "PatientDosageTitleLabel";
            this.PatientDosageTitleLabel.Size = new System.Drawing.Size(123, 18);
            this.PatientDosageTitleLabel.TabIndex = 0;
            this.PatientDosageTitleLabel.Text = "Patient Dosage";
            // 
            // DosageTextLabel
            // 
            this.DosageTextLabel.AutoSize = true;
            this.DosageTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DosageTextLabel.Location = new System.Drawing.Point(397, 380);
            this.DosageTextLabel.Name = "DosageTextLabel";
            this.DosageTextLabel.Size = new System.Drawing.Size(99, 25);
            this.DosageTextLabel.TabIndex = 1;
            this.DosageTextLabel.Text = "Dosage:";
            // 
            // DosageAmountLabel
            // 
            this.DosageAmountLabel.AutoSize = true;
            this.DosageAmountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DosageAmountLabel.Location = new System.Drawing.Point(490, 380);
            this.DosageAmountLabel.Name = "DosageAmountLabel";
            this.DosageAmountLabel.Size = new System.Drawing.Size(25, 25);
            this.DosageAmountLabel.TabIndex = 2;
            this.DosageAmountLabel.Text = "0";
            // 
            // BeginDateCalender
            // 
            this.BeginDateCalender.Location = new System.Drawing.Point(124, 36);
            this.BeginDateCalender.MaxSelectionCount = 1;
            this.BeginDateCalender.Name = "BeginDateCalender";
            this.BeginDateCalender.TabIndex = 3;
            // 
            // BeginDateLabel
            // 
            this.BeginDateLabel.AutoSize = true;
            this.BeginDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BeginDateLabel.Location = new System.Drawing.Point(13, 104);
            this.BeginDateLabel.Name = "BeginDateLabel";
            this.BeginDateLabel.Size = new System.Drawing.Size(99, 20);
            this.BeginDateLabel.TabIndex = 4;
            this.BeginDateLabel.Text = "Begin Date";
            // 
            // EndDateLabel
            // 
            this.EndDateLabel.AutoSize = true;
            this.EndDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndDateLabel.Location = new System.Drawing.Point(13, 301);
            this.EndDateLabel.Name = "EndDateLabel";
            this.EndDateLabel.Size = new System.Drawing.Size(85, 20);
            this.EndDateLabel.TabIndex = 5;
            this.EndDateLabel.Text = "End Date";
            // 
            // EndDateCalender
            // 
            this.EndDateCalender.Location = new System.Drawing.Point(124, 209);
            this.EndDateCalender.MaxSelectionCount = 1;
            this.EndDateCalender.Name = "EndDateCalender";
            this.EndDateCalender.TabIndex = 6;
            // 
            // PatientIdTextBox
            // 
            this.PatientIdTextBox.Location = new System.Drawing.Point(439, 36);
            this.PatientIdTextBox.Name = "PatientIdTextBox";
            this.PatientIdTextBox.Size = new System.Drawing.Size(100, 20);
            this.PatientIdTextBox.TabIndex = 7;
            // 
            // GlucoseLevelTextBox
            // 
            this.GlucoseLevelTextBox.Location = new System.Drawing.Point(439, 84);
            this.GlucoseLevelTextBox.Name = "GlucoseLevelTextBox";
            this.GlucoseLevelTextBox.Size = new System.Drawing.Size(100, 20);
            this.GlucoseLevelTextBox.TabIndex = 8;
            // 
            // PatientIdLabel
            // 
            this.PatientIdLabel.AutoSize = true;
            this.PatientIdLabel.Location = new System.Drawing.Point(381, 43);
            this.PatientIdLabel.Name = "PatientIdLabel";
            this.PatientIdLabel.Size = new System.Drawing.Size(52, 13);
            this.PatientIdLabel.TabIndex = 9;
            this.PatientIdLabel.Text = "Patient Id";
            // 
            // GlucoseLevelLabel
            // 
            this.GlucoseLevelLabel.AutoSize = true;
            this.GlucoseLevelLabel.Location = new System.Drawing.Point(358, 87);
            this.GlucoseLevelLabel.Name = "GlucoseLevelLabel";
            this.GlucoseLevelLabel.Size = new System.Drawing.Size(75, 13);
            this.GlucoseLevelLabel.TabIndex = 10;
            this.GlucoseLevelLabel.Text = "Glucose Level";
            // 
            // GetDosageButton
            // 
            this.GetDosageButton.Location = new System.Drawing.Point(439, 127);
            this.GetDosageButton.Name = "GetDosageButton";
            this.GetDosageButton.Size = new System.Drawing.Size(100, 24);
            this.GetDosageButton.TabIndex = 11;
            this.GetDosageButton.Text = "Get Dosage Amount";
            this.GetDosageButton.UseVisualStyleBackColor = true;
            this.GetDosageButton.Click += new System.EventHandler(this.GetDosageButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(17, 406);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 12;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = true;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // PreviousButton
            // 
            this.PreviousButton.Location = new System.Drawing.Point(358, 348);
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(75, 23);
            this.PreviousButton.TabIndex = 13;
            this.PreviousButton.Text = "Previous";
            this.PreviousButton.UseVisualStyleBackColor = true;
            // 
            // NextButton
            // 
            this.NextButton.Location = new System.Drawing.Point(358, 319);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(75, 23);
            this.NextButton.TabIndex = 14;
            this.NextButton.Text = "Next";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // NewButton
            // 
            this.NewButton.Location = new System.Drawing.Point(358, 232);
            this.NewButton.Name = "NewButton";
            this.NewButton.Size = new System.Drawing.Size(77, 23);
            this.NewButton.TabIndex = 15;
            this.NewButton.Text = "New";
            this.NewButton.UseVisualStyleBackColor = true;
            this.NewButton.Click += new System.EventHandler(this.NewButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(358, 290);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 16;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(358, 261);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 17;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // PatientDosage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 441);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.NewButton);
            this.Controls.Add(this.NextButton);
            this.Controls.Add(this.PreviousButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.GetDosageButton);
            this.Controls.Add(this.GlucoseLevelLabel);
            this.Controls.Add(this.PatientIdLabel);
            this.Controls.Add(this.GlucoseLevelTextBox);
            this.Controls.Add(this.PatientIdTextBox);
            this.Controls.Add(this.EndDateCalender);
            this.Controls.Add(this.EndDateLabel);
            this.Controls.Add(this.BeginDateLabel);
            this.Controls.Add(this.BeginDateCalender);
            this.Controls.Add(this.DosageAmountLabel);
            this.Controls.Add(this.DosageTextLabel);
            this.Controls.Add(this.PatientDosageTitleLabel);
            this.Name = "PatientDosage";
            this.Text = "PatientDosage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PatientDosageTitleLabel;
        private System.Windows.Forms.Label DosageTextLabel;
        private System.Windows.Forms.Label DosageAmountLabel;
        private System.Windows.Forms.MonthCalendar BeginDateCalender;
        private System.Windows.Forms.Label BeginDateLabel;
        private System.Windows.Forms.Label EndDateLabel;
        private System.Windows.Forms.MonthCalendar EndDateCalender;
        private System.Windows.Forms.TextBox PatientIdTextBox;
        private System.Windows.Forms.TextBox GlucoseLevelTextBox;
        private System.Windows.Forms.Label PatientIdLabel;
        private System.Windows.Forms.Label GlucoseLevelLabel;
        private System.Windows.Forms.Button GetDosageButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button PreviousButton;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button NewButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button SaveButton;
    }
}