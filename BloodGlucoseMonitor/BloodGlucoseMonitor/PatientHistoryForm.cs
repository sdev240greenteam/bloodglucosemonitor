﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BloodGlucoseMonitorWin
{
    public partial class PatientHistory : Form
    {
        public PatientHistory()
        {
            InitializeComponent();
        }

        private void AddressLabel_Click(object sender, EventArgs e)
        {

        }

        private void ResultButton_Click(object sender, EventArgs e)
        {
            GetHistory();
        }

        private void GetHistory()
        {
            int patientId;
            int dosageId;

            //CHECK IF THE VALUES ENTERED ARE VALID
            if(Int32.TryParse(PatientIdTextBox.Text, out patientId))
            {
                if(Int32.TryParse(DosageScheduleIdTextBox.Text, out dosageId))
                {
                    //--------------------------------------------------------

                    //CREATE THE PATIENT HISTORY INTO A CLASS SO IT IS READY TO BE PLACED IN DATABASE
                    PatientHistory history = new PatientHistory
                    {
                        PatientID = patientId,
                        DateEntered = PatientCalender.SelectionRange.Start,
                        DosageScheduleID = dosageId
                    };

                    //ALWAYS MAKE SURE IF ENTERED CORRECTLY, LABELS ARE BLACK
                    DosageScheduleIdTextBox.ForeColor = Color.Black;
                    PatientIdLabel.ForeColor = Color.Black;
                }
                //IF INVALID, MAKES THE COLORS OF LABELS RED TO INDICATE WRONG INPUT
                else
                {
                    DosageScheduleIdTextBox.ForeColor = Color.Red;
                }
            }
            else
            {
                PatientIdLabel.ForeColor = Color.Red;
            }
            //-------------------------------------------------

        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewButton_Click(object sender, EventArgs e)
        {
            //CLEAR VALUES TO ALLOW NEW ENTRY
            PatientIdTextBox.Text = "";
            DosageScheduleIdTextBox.Text = "";
        }
    }
}
