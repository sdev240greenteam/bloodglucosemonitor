﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodGlucoseMonitorWin
{
    partial class PatientHistory
    {
        public int PatientID { get; set; }
        public DateTime DateEntered { get; set; }
        public int DosageScheduleID { get; set; }
        public float Result { get; set; }
    }
}
